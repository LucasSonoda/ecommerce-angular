import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './components/admin/admin.component';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
  {path:'main', component:MainComponent}
  ,{path:'administrar',component:AdminComponent}
  , {path:'', redirectTo:'home', pathMatch: 'full'}
  , {path:'**', component: MainComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
